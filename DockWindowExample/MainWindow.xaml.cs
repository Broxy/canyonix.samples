﻿using System.Windows;
using Canyonix.UI.Windows;

namespace DockWindowExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Float_OnClick(object sender, RoutedEventArgs e)
        {
            Float();
        }

        private void Hide_OnClick(object sender, RoutedEventArgs e)
        {
            Collapse();
        }

        private void Right_OnClick(object sender, RoutedEventArgs e)
        {
            Dock(DockingEdge.Right);
        }

        private void Left_OnClick(object sender, RoutedEventArgs e)
        {
            Dock(DockingEdge.Left);
        }

        private void Top_OnClick(object sender, RoutedEventArgs e)
        {
            Dock(DockingEdge.Top);
        }

        private void Pin_OnClick(object sender, RoutedEventArgs e)
        {
            Pin(); 
        }

        private void Unpin_OnClick(object sender, RoutedEventArgs e)
        {
            Unpin(); 
        }


    }
}
