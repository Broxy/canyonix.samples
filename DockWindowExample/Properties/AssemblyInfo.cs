﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DockWindowExample")]
[assembly: AssemblyDescription("Example for creating a dockable window app on WPF")]
[assembly: AssemblyCompany("Canyonix")]
[assembly: AssemblyProduct("Canyonix Samples")]
[assembly: AssemblyCopyright("Copyright 2015 Canyonix")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
